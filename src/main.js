import { createApp } from 'vue'

import App from './App.vue'
import BaseWidget from './components/UI/BaseWidget.vue'
import BaseCard from './components/UI/BaseCard.vue'
import BaseListGrid from './components/Layout/BaseListGrid.vue'
import BaseElementGrid from './components/Layout/BaseElementGrid.vue'


const app = createApp(App);

app.component('base-widget', BaseWidget);
app.component('base-card', BaseCard);
app.component('base-list-grid', BaseListGrid);
app.component('base-element-grid', BaseElementGrid);

app.mount('#app');
